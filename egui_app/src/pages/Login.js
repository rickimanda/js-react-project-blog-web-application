import { useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import LoginForm from "../components/login_components/LoginForm";
import LoginContext from "../store/login-context";

function LoginPage() {
  const navigate = useNavigate();
  const loginContext = useContext(LoginContext);

  
  const [isLoading, setIsLoading] = useState(true);
  const [loadedUsers, setLoadedUsers] = useState([]);
  const [loginFailed, setLoginFailed] = useState("");

  // get data from user database
  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8000/users")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setIsLoading(false);
        setLoadedUsers(data);
      });
  }, []);

  function loginHandler(loginData) {

    // check if good login data
    if ( ! loadedUsers.some( user => user.email === loginData.email && user.password === loginData.password)) {
      setLoginFailed("Login failed. Incorrect username or password.");
      return; }

      setLoginFailed("");

    // change app-wide state (context) and go to blogs page
    loginContext.logIn(loginData.email);
    navigate("/my-blogs");
  }

  function logoutHandler() {
    loginContext.logOut();
  }

  function registerRedirect()
  {
    navigate("/register");
  }

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  if (loginContext.isLoggedIn) {
    logoutHandler();
  }

  return (
    <section>
      <h1>Log In</h1>
      <LoginForm 
        onLogin={loginHandler}
        onFailure={loginFailed}
        onRedirect={registerRedirect}
        />
    </section>
  );
}

export default LoginPage;
