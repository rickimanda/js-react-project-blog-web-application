import { useNavigate } from "react-router-dom";
import NewBlogForm from "../components/blogs/NewBlogForm";

function NewBlogPage() {
    const navigate = useNavigate();

  function addBlogHandler(blogData) {
    fetch("http://localhost:8000/blogs", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(blogData),
    }).then(
        () => {
            navigate("/my-blogs", {replace:true});
        }
    );
  }

  return (
    <section>
      <h1>Create New Blog</h1>
      <NewBlogForm onAddBlog={addBlogHandler} />
    </section>
  );
}

export default NewBlogPage;
