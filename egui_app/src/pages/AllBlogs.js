import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";

import BlogList from "../components/blogs/BlogList";
import LoginContext from "../store/login-context";
import BlogContext from "../store/blog-context";

function AllBlogsPage() {
  const [isLoading, setIsLoading] = useState(true);
  const [loadedBlogs, setLoadedBlogs] = useState([]);

  const loginCtx = useContext(LoginContext);
  const loggedUser = loginCtx.loggedUser;

  const blogCtx = useContext(BlogContext);

  const navigate = useNavigate();

  function notLoggedHandler() {
    navigate("/login");
  }

  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8000/blogs")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setIsLoading(false);
        setLoadedBlogs(data);
      });
  }, []);
  //  ^ external dependencies - user components etc., eg. props. not states.

  const displayedBlogs = [];

  loadedBlogs.forEach((blog) => {
    if (blog.email === loggedUser) displayedBlogs.push(blog);
  });

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  if (!loginCtx.isLoggedIn) {
    notLoggedHandler();
  }

  function goToHandler(blogId) {
    blogCtx.currentBlog = blogId;
    navigate(`/my-blogs/${blogCtx.currentBlog}`);
  }

  function editHandler(blogId, blogTitle) {
    blogCtx.currentBlog = blogId;

    var currInd = 0;

    loadedBlogs.forEach((blog, index) => {
        if (blog.id === blogCtx.currentBlog) {
          blog.title = blogTitle;
          currInd = index;
        }
      });

    setIsLoading(true);

    fetch("http://localhost:8000/blogs/" + blogCtx.currentBlog, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loadedBlogs[currInd]),
      })
      .then((response) => {
        return response.json();
      })
      .then( () => {
          // setIsAdding(false);
          setIsLoading(false); }
      );

  }

  function deleteHandler(blogId) {
    blogCtx.currentBlog = blogId;

    setIsLoading(true);

    fetch("http://localhost:8000/blogs/" + blogCtx.currentBlog, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        }
      })
      .then((response) => {
        return response.json();
      })
      .then(() => {
        return fetch("http://localhost:8000/blogs")
      })
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          setIsLoading(false);
          setLoadedBlogs(data);
        });
  }

  return (
    <section>
      <h1>My Blogs:</h1>
      { (displayedBlogs.length === 0) && <h2>You have not created any blogs yet.</h2> }
      <BlogList blogs={displayedBlogs} 
        onGoTo={goToHandler} 
        onEdit={editHandler}
        onDelete={deleteHandler}
        />
    </section>
  );
}

export default AllBlogsPage;
