import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import RegisterForm from "../components/login_components/RegisterForm";

function RegisterPage() {
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(true);
  const [loadedUsers, setLoadedUsers] = useState([]);
  const [failureMsg, setFailureMsg] = useState("");

  // get data from user database
  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8000/users")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setIsLoading(false);
        setLoadedUsers(data);
      });
  }, []);

  function registerHandler(registerData, isRepeatOk) {
    // checks for correct registration
    if (!isRepeatOk) {
      setFailureMsg("Passwords don't match!");
      return;
    }

    if (!registerData.email.includes("@")) {
      setFailureMsg("Invalid e-mail. Must contain @!");
      return;
    }

    if (loadedUsers.some((user) => user.email === registerData.email)) {
      setFailureMsg("Email already registered!");
      return;
    }

    // write new user to db then go to login page
    fetch("http://localhost:8000/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(registerData),
    }).then(() => {
      navigate("/login", { replace: true });
    });
  }

  function loginRedirect() {
    navigate("/login", { replace: true });
  }

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  return (
    <section>
      <h1>Register</h1>
      <RegisterForm 
        onRegister={registerHandler} 
        onFailure={failureMsg} 
        onRedirect={loginRedirect} />
    </section>
  );
}

export default RegisterPage;
