import { useState, useEffect, useContext } from "react";
import BlogEntryList from "../components/blog_entries/BlogEntryList";
import BlogContext from "../store/blog-context";
import NewBlogEntryForm from "../components/blog_entries/NewBlogEntryForm";

function BlogPage() {

    const [isLoading, setIsLoading] = useState(true);
  const [loadedBlogs, setLoadedBlogs] = useState([]);
  const [isAdding, setIsAdding] = useState(false);

  const blogCtx = useContext(BlogContext);

  useEffect(() => {
    setIsLoading(true);
    fetch("http://localhost:8000/blogs")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setIsLoading(false);
        setLoadedBlogs(data);
      });
  }, []);

  if (isLoading) {
    return (
      <section>
        <p>Loading...</p>
      </section>
    );
  }

  const displayedBlogs = [];

  loadedBlogs.forEach((blog) => {
    if (blog.id === blogCtx.currentBlog) displayedBlogs.push(blog);
  });

  function clickAddEntryHandler() {
    setIsAdding(true);
  }

  function cancelAddEntryHandler() {
    setIsAdding(false);
  }

  function addEntryHandler(entryData) {

    var currInd = 0;

    loadedBlogs.forEach((blog, index) => {

        if (blog.id === blogCtx.currentBlog) {
            blog.entries.push({id: ( blog.entries.length !== 0 ? blog.entries[blog.entries.length-1].id+1 : 0), 
              title: entryData.title, content:entryData.content, date: entryData.date});
            blog.nrEntries = blog.entries.length;
            currInd = index;
        }
      });

    setIsLoading(true);

    fetch("http://localhost:8000/blogs/" + blogCtx.currentBlog, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loadedBlogs[currInd]),
      })
      .then((response) => {
        return response.json();
      })
      .then( () => {
          setIsAdding(false);
          setIsLoading(false); }
      );
      
  }

  function deleteHandler(entryId) {

    var currInd = 0;

    loadedBlogs.forEach((blog, index) => {
        if (blog.id === blogCtx.currentBlog) {
          currInd = index;
        }
      });

      loadedBlogs[currInd].entries.forEach((entry, index) => {
        if (entry.id === entryId)
          loadedBlogs[currInd].entries.splice(index, 1)
      })

    setIsLoading(true);

    fetch("http://localhost:8000/blogs/" + blogCtx.currentBlog, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loadedBlogs[currInd]),
      })
      .then((response) => {
        return response.json();
      })
      .then( () => {
          // setIsAdding(false);
          setIsLoading(false); }
      );

  }

  function editHandler(entryId, entryData) {
        var currInd = 0;

    loadedBlogs.some((blog, index) => {
          currInd = index;
          return (blog.id === blogCtx.currentBlog);
      });

      loadedBlogs[currInd].entries.forEach((entry) => {
        if (entry.id === entryId) {
        entry.title = entryData.title;
        entry.content = entryData.content; }
      })

    setIsLoading(true);

    fetch("http://localhost:8000/blogs/" + blogCtx.currentBlog, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loadedBlogs[currInd]),
      })
      .then((response) => {
        return response.json();
      })
      .then( () => {
          // setIsAdding(false);
          setIsLoading(false); }
      );
    
  }


    return (
    <section>
      <h1>{displayedBlogs[0].title}</h1>
      <h3>Entries: {displayedBlogs[0].nrEntries}</h3>
      {<NewBlogEntryForm 
        onAdding={isAdding} 
        onClickAdd = {clickAddEntryHandler}
        onClickCancel = {cancelAddEntryHandler}
        onAddEntry={addEntryHandler} />}
      <BlogEntryList blogEntries={displayedBlogs[0].entries} 
        onEdit={editHandler}
        onDelete={deleteHandler}
      />
    </section>
    );
}

export default BlogPage;
