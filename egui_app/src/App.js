import { Route, Routes } from "react-router-dom";

import AllBlogsPage from "./pages/AllBlogs";
import NewBlogPage from "./pages/NewBlog";
import LoginPage from "./pages/Login";
import RegisterPage from "./pages/Register";
import BlogPage from "./pages/Blog";
import Layout from "./components/layout/Layout";

function App() {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<LoginPage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/my-blogs" element={<AllBlogsPage />} />
        <Route path="/new-blog" element={<NewBlogPage />} />
        <Route path="/my-blogs/:blog" exactly element={<BlogPage />} /> 
      </Routes>
    </Layout>
  );
}

export default App;
