import { createContext, useState } from "react";

const BlogContext = createContext({ 
    currentBlog : "",
    goToBlog : (goToHandler) => {}, 
    goFromBlog : (goFromHandler) => {} });

export function BlogContextProvider(props) {

    const [currentBlogName, setCurrentBlogName] = useState("");

    function goToHandler(blogName) {
        setCurrentBlogName(blogName);
    }

    function goFromHandler() {
        setCurrentBlogName("");
    }

    const context = {
        currentBlog : currentBlogName,
        goToBlog : goToHandler,
        goFromBlog : goFromHandler
    };

    return <BlogContext.Provider value={context}>
        {props.children}
    </BlogContext.Provider>
}

export default BlogContext;