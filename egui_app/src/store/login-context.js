import { createContext, useState } from "react";

const LoginContext = createContext({ isLoggedIn : false, loggedUser : "", logIn : (loginHandler) => {}, logOut : (logoutHandler) => {} });

export function LoginContextProvider(props) {

    const [isUserLogged, setIsUserLogged] = useState(false);
    const [userLoggedName, setUserLoggedName] = useState("");

    function loginHandler(username) {
        setIsUserLogged(true);
        setUserLoggedName(username);
    }

    function logoutHandler() {
        setIsUserLogged(false);
        setUserLoggedName("");
    }

    const context = {
        isLoggedIn : isUserLogged,
        loggedUser : userLoggedName,
        logIn : loginHandler,
        logOut : logoutHandler
    };

    return <LoginContext.Provider value={context}>
        {props.children}
    </LoginContext.Provider>
}

export default LoginContext;