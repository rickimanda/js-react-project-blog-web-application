import { useRef, useState } from "react";
import Card from "../ui/Card";
import classes from "./NewBlogForm.module.css";

function EditBlogForm(props) {
  const titleInputRef = useRef();
  const [state, setState] = useState(props.title);

  const handleChange = e => {
    setState( e.target.value);
  };

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredTitle = titleInputRef.current.value;

    props.onEditBlog(enteredTitle);
  }

  function cancelHandler()
  {
    props.onCancel();
  }

  return (
    <Card>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor="title">New blog title</label>
          <input type="text" required id="title" ref={titleInputRef} value={state}
            onChange={handleChange}/>
        </div>
        <div className={classes.actions}>
          <button type="button" onClick={cancelHandler}>Cancel</button>
          <button>Change</button>
        </div>
      </form>
    </Card>
  );
}

export default EditBlogForm;
