import { useRef, useContext } from "react";
import LoginContext from "../../store/login-context";
import Card from "../ui/Card";
import classes from "./NewBlogForm.module.css";

function NewBlogForm(props) {
  const titleInputRef = useRef();
  const loginCtx = useContext(LoginContext);
  const loggedUser = loginCtx.loggedUser;

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredTitle = titleInputRef.current.value;

    const blogData = {
      title: enteredTitle,
      email: loggedUser,
      nrEntries: 0,
      entries: [],
    };

    props.onAddBlog(blogData);
  }

  return (
    <Card>
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor="title">Blog title</label>
          <input type="text" required id="title" ref={titleInputRef} />
        </div>
        <div className={classes.actions}>
          <button>Create</button>
        </div>
      </form>
    </Card>
  );
}

export default NewBlogForm;
