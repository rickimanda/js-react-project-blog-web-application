import classes from "./BlogItem.module.css";
import Card from "../ui/Card";
import { useState } from "react";
import EditBlogForm from "./EditBlogForm";

function BlogItem(props) {
  const [isEditing, setIsEditing] = useState(false);
  const blogLink = props.id;


  function goToHandler() {
    props.onGoTo(blogLink);
  }

  function clickEditHandler() {
    setIsEditing(true);
  }

  function cancelHandler() {
    setIsEditing(false);
  }

  function editHandler (enteredTitle){
    setIsEditing(false);
    props.onEdit(blogLink, enteredTitle);
  }

  function deleteHandler() {
    props.onDelete(blogLink);
  }

  return (
    <li className={classes.item}>
     { ! isEditing && (
     <Card>
        <div className={classes.content}>
          <h2> {props.title} </h2>
        </div>
        <div className={classes.actions}>
          <button onClick={goToHandler}>Go to blog</button>
          <button onClick={clickEditHandler}>Edit title</button>
          <button onClick={deleteHandler}>Delete</button>
        </div>
      </Card> )}
      { isEditing &&
        (<EditBlogForm title={props.title} onEditBlog={editHandler} onCancel={cancelHandler} />
      )}
    </li>
  );
}

export default BlogItem;
