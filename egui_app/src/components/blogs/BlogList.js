import classes from './BlogList.module.css';

import BlogItem from "./BlogItem";

function BlogList(props) {
  
  function goToHandler(blogId) {
    props.onGoTo(blogId);
  }

  function editHandler(blogId, blogTitle) {
    props.onEdit(blogId, blogTitle);
  }

  function deleteHandler(blogId) {
    props.onDelete(blogId);
  }


  return (
    <ul className={classes.list}>
      {props.blogs.map((blog) => (
        <BlogItem key={blog.id} 
            id = {blog.id}
            title={blog.title}
            email={blog.email}
            nrEntries={blog.nrEntries}
            onGoTo={goToHandler}
            onEdit={editHandler}
            onDelete={deleteHandler}
        />
      ))}
    </ul>
  );
}

export default BlogList;
