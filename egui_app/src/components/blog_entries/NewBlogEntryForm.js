import { useRef } from "react";
import Card from "../ui/Card";
import classes from "./NewBlogEntryForm.module.css";

function NewBlogEntryForm(props) {
  const titleInputRef = useRef();
  const contentInputRef = useRef();

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredTitle = titleInputRef.current.value;
    const enteredContent = contentInputRef.current.value;
    const currDate = new Date().toLocaleDateString("de");

    const entryData = {
      title: enteredTitle,
      content: enteredContent,
      date: currDate,
    };

    props.onAddEntry(entryData);
  }

  return (
    <>
      <div className={classes.actions1}>
        {!props.onAdding && (
          <button type="button" onClick={props.onClickAdd}>
            Add new entry
          </button>
        )}
        {props.onAdding && (
          <button type="button" onClick={props.onClickCancel}>
            Cancel
          </button>
        )}
      </div>

      {props.onAdding && (
        <Card>
          <form className={classes.form} onSubmit={submitHandler}>
            <div className={classes.control}>
              <label htmlFor="title">New entry title</label>
              <input type="text" required id="title" ref={titleInputRef} />
            </div>
            <div className={classes.control}>
              <label htmlFor="content">New entry content</label>
              <textarea id="content" required rows="5" ref={contentInputRef} />
            </div>
            <div className={classes.actions}>
              <button>Create</button>
            </div>
          </form>
        </Card>
      )}
    </>
  );
}

export default NewBlogEntryForm;
