import BlogEntryItem from "./BlogEntryItem";
import classes from './BlogEntryList.module.css';

function BlogEntryList(props) {

  function editHandler(blogId, blogData) {
    props.onEdit(blogId, blogData);
  }

  function deleteHandler(blogId) {
    props.onDelete(blogId);
  }

  return (
    <ul className={classes.list}>
      {props.blogEntries.map((blogEntry) => (
        <BlogEntryItem key={blogEntry.id} 
            id={blogEntry.id} 
            title={blogEntry.title}
            content={blogEntry.content}
            date={blogEntry.date}
            onEdit={editHandler}
            onDelete={deleteHandler}
        />
      ))}
    </ul>
  );
}

export default BlogEntryList;