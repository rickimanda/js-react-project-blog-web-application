import classes from "./BlogEntryItem.module.css";
import Card from "../ui/Card";
import { useState } from "react";
import EditBlogEntryForm from "./EditBlogEntryForm";

function BlogEntryItem(props) {
  const [isEditing, setIsEditing] = useState(false);
  const entryLink = props.id;

  function clickEditHandler() {
    setIsEditing(true);
  }

  function cancelHandler() {
    setIsEditing(false);
  }

  function editHandler (entryData){
    setIsEditing(false);
    props.onEdit(entryLink, entryData);
  }

  function deleteHandler() {
    props.onDelete(entryLink);
  }

  return (
    <li className={classes.item}>
      { !isEditing && (
        <Card>
        <div className={classes.content}>
          <h2>{props.title} </h2>
          <p>{props.date} </p>
          <h3>{props.content}</h3>
        </div>
        <div className={classes.actions}>
          <button onClick={clickEditHandler}>Edit entry</button>
          <button onClick={deleteHandler}>Delete</button>
        </div>
      </Card> )}
      {isEditing && (
        <EditBlogEntryForm title={props.title}
        content={props.content}
        onEditEntry={editHandler}
        onCancel={cancelHandler}
        />
      )}
    </li>
  );
}

export default BlogEntryItem;
