import { useRef, useState } from "react";
import Card from "../ui/Card";
import classes from "./NewBlogEntryForm.module.css";

function EditBlogEntryForm(props) {
  const titleInputRef = useRef();
  const contentInputRef = useRef();
  const [stateT, setStateT] = useState(props.title);
  const [stateC, setStateC] = useState(props.content);

  const handleChangeT = (e) => {
    setStateT(e.target.value);
  };
  const handleChangeC = (e) => {
    setStateC(e.target.value);
  };

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredTitle = titleInputRef.current.value;
    const enteredContent = contentInputRef.current.value;

    const entryData = {
      title: enteredTitle,
      content: enteredContent,
    };

    props.onEditEntry(entryData);
  }

  function cancelHandler() {
    props.onCancel();
  }

  return (
        <Card>
          <form className={classes.form} onSubmit={submitHandler}>
            <div className={classes.control}>
              <label htmlFor="title">New entry title</label>
              <input type="text" required id="title" ref={titleInputRef}
              value={stateT} onChange={handleChangeT} />
            </div>
            <div className={classes.control}>
              <label htmlFor="content">New entry content</label>
              <textarea id="content" required rows="5" ref={contentInputRef}
              value={stateC} onChange={handleChangeC} />
            </div>
            <div className={classes.actions}>
              <button type="button" onClick={cancelHandler}>
                Cancel
              </button>
              <button>Change</button>
            </div>
          </form>
        </Card>
      );
}

export default EditBlogEntryForm;
