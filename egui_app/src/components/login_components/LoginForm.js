import { useRef } from "react";
import Card from "../ui/Card";
import classes from "./LoginForm.module.css";

function LoginForm(props) {
  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    const loginData = { email: enteredEmail, password: enteredPassword };

    props.onLogin(loginData);
  }

  return (
    <div>
      <Card>
        <form className={classes.form} onSubmit={submitHandler}>
          <div className={classes.control}>
            <label htmlFor="email">E-mail:</label>
            <input type="text" required ref={emailInputRef} />
          </div>
          <div className={classes.control}>
            <label htmlFor="password">Password:</label>
            <input type="password" required ref={passwordInputRef} />
          </div>
          <div className={classes.actions}>
            <button>Log In</button>
          </div>
          <div className={classes.control}>
            <p>{props.onFailure}</p>
          </div>
        </form>
      </Card>
      <h1>Don't have an account yet?</h1>
      <div className={classes.actions}>
      <button type="button" onClick={props.onRedirect}>Register</button>
      </div>
    </div>
  );
}

export default LoginForm;
