import { useRef } from "react";
import Card from "../ui/Card";
import classes from "./RegisterForm.module.css";

function RegisterForm(props) {
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const repeatInputRef = useRef();

  function submitHandler(event) {
    event.preventDefault(); // default would be http request

    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;
    const enteredRepeat = repeatInputRef.current.value;

    const registerData = { email: enteredEmail, password: enteredPassword };
    const isRepeatOk = enteredPassword === enteredRepeat ? true : false;

    props.onRegister(registerData, isRepeatOk);
  }

  return (
    <div>
      <Card>
        <form className={classes.form} onSubmit={submitHandler}>
          <div className={classes.control}>
            <label htmlFor="email">E-mail:</label>
            <input type="text" required ref={emailInputRef} />
          </div>
          <div className={classes.control}>
            <label htmlFor="password">Password:</label>
            <input type="password" required ref={passwordInputRef} />
          </div>
          <div className={classes.control}>
            <label htmlFor="repeat">Repeat password:</label>
            <input type="password" required ref={repeatInputRef} />
          </div>
          {/* <textarea> id="description" required rows='5' </textarea> */}
          <div className={classes.actions}>
            <button>Register</button>
          </div>
          <div className={classes.control}>
            <p>{props.onFailure}</p>
          </div>
        </form>
      </Card>
      <h1>Already have an account?</h1>
      <div className={classes.actions}>
      <button type="button" onClick={props.onRedirect}>Log In</button>
      </div>
    </div>
  );
}

export default RegisterForm;
