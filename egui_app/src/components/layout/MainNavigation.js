import { Link} from "react-router-dom";
import {useContext} from 'react';
import LoginContext from "../../store/login-context";

import classes from "./MainNavigation.module.css"

function MainNavigation(props) {

  const loginContext = useContext(LoginContext);
  console.log(loginContext);


  function logoutHandler() {
    loginContext.logOut();
  }

  return (
    <header className={classes.header}>
      <div className={classes.logo}>EGUI Blogging React Application</div>
      {props.isLoggedIn &&
      <div>
        <nav>
          <ul>
              <p>{props.loggedUser}</p>
            <li>
              <Link to='/my-blogs'>My Blogs</Link>
            </li>
            <li>
              <Link to='/new-blog'>Create New Blog</Link>
            </li>
            <li>
              <Link to='/login' onClick={logoutHandler}>Log Out</Link>
            </li>
          </ul>
        </nav>
      </div>
      } 
    </header>
  );
}

export default MainNavigation;
