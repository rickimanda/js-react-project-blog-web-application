import classes from './Layout.module.css';
import MainNavigation from './MainNavigation';
import { useContext } from 'react';
import LoginContext from '../../store/login-context';

function Layout (props) {
    const loginCtx = useContext(LoginContext);

    return <div>
        <MainNavigation isLoggedIn={loginCtx.isLoggedIn} loggedUser={loginCtx.loggedUser} /> 
        <main className={classes.main}>
            {props.children}
        </main>
    </div> 
}

export default Layout;